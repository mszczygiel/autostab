# AutoStab
Automatically update your remote Git repositories. It's at very early stage of development. But may be useful to somebody already.

## Install
Ensure you have **JRE version 11** installed

Download [autostab-0.0.1.tgz](/uploads/08318803baa678683c5f799494974991/autostab-0.0.1.tgz) and extract archive

`tar -xvf autostab-0.0.1.tgz`

## Usage

### Create spec file
By default file named `autostab-spec.json` is used

Example:
```json
{
  "repos": [
    {
      "projectId": 12015991,
      "url": "git@gitlab.com:mszczygiel/autostab.git"
    }
  ],
  "pathRegex": ".*\\.sbt$",
  "branchName": "autostab-update",
  "commitMessage": "[AUTOSTAB] versions were updated",
  "mrTitle": "[AUTOSTAB] MR title",
  "replacements": [
    {
      "regex": "(?m)^.*cats-core.*$",
      "replaceWith": "  \"org.typelevel\" %% \"cats-core\" % \"1.6.0\","
    },
    {
      "regex": "(?m)^.*sbt-native-packager.*$",
      "replaceWith": "addSbtPlugin(\"com.typesafe.sbt\" % \"sbt-native-packager\" % \"1.3.20\")"
    }
  ]
}
```
- `repos` - list of repositories to update. `projectId` **must** match `url` - there is no verification in place. `url` must be URL that you are allowed to push to 
- `pathRegex` - only files with names matching this regex will be updated
- `branchName` - branch that will be created and pushed in case any update took place
- `replacements` - specification of replacements, where each matching `regex` will be replaced with `replaceWith` 

### Run application
`autostab-0.0.1/bin/autostab --token <GITLAB_TOKEN> --gitlab-url https://gitlab.com --spec-file spec.json`

### Regular expressions format
Regular expressions are compatible with [java.util.regex.Pattern](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html)


## Known issues
- Repositories are cloned to local temp. Currently created directories will not be cleaned. You may clean them yourself by
`rm -rf /tmp/autostab*`

## License
AutoStab is made available under the Apache 2.0 license.
