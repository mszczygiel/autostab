package autostab

import io.circe.{Decoder, Encoder}
import cats.effect.{IO, Resource}
import org.http4s.{Header, Method, Request, Uri}
import org.http4s.client._
import org.http4s.circe._
import cats.syntax.all._
import org.slf4j.LoggerFactory

trait GitLab[F[_]] {
  def createMergeRequest(projectId: GitLab.ProjectId,
                         sourceBranch: GitLab.BranchName,
                         targetBranch: GitLab.BranchName,
                         title: String): F[Unit]
}

object GitLab {
  private val log = LoggerFactory.getLogger(GitLab.getClass)

  final case class ProjectId(value: Long) extends AnyVal
  final case class BranchName(value: String) extends AnyVal
  final case class Project(id: Long, url: String)
  final case class Token(value: String) extends AnyVal

  final case class GitLabUrl(value: String) extends AnyVal

  def instance(token: Token, gitLabUrl: GitLabUrl, client: Resource[IO, Client[IO]]): GitLab[IO] =
    new GitLab[IO] with CirceEntityEncoder {
      private val GitLabUri = Uri.unsafeFromString(gitLabUrl.value)

      override def createMergeRequest(projectId: ProjectId,
                                      sourceBranch: BranchName,
                                      targetBranch: BranchName,
                                      title: String): IO[Unit] = {
        IO(log.info(
          s"creating MR ${sourceBranch.value} -> ${targetBranch.value} in project ${projectId.value}")) *> client
          .use { http =>
            http.expect[Unit](
              Request[IO](
                method = Method.POST,
                uri = GitLabUri.withPath(s"/api/v4/projects/${projectId.value}/merge_requests"),
              ).withEntity(requests
                  .CreateMRRequest(sourceBranch.value, targetBranch.value, title, true))
                .authenticated)
          }
      }

      private implicit class AuthenticatedGitLabRequest[F[_]](request: Request[F]) {
        def authenticated: Request[F] = request.putHeaders(Header("PRIVATE-TOKEN", token.value))
      }
    }

  private object requests {
    final case class CreateMRRequest(sourceBranch: String,
                                     targetBranch: String,
                                     title: String,
                                     removeSourceBranch: Boolean)

    object CreateMRRequest {
      implicit val createMrRequestEncoder: Encoder[CreateMRRequest] =
        Encoder.forProduct4("source_branch", "target_branch", "title", "remove_source_branch")(r =>
          (r.sourceBranch, r.targetBranch, r.title, r.removeSourceBranch))

      implicit val createMRRequestDecoder: Decoder[CreateMRRequest] =
        Decoder.forProduct4("source_branch", "target_branch", "title", "remove_source_branch")(
          CreateMRRequest.apply)
    }
  }
}
