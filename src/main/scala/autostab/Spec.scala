package autostab

import java.io.File

import cats.effect.IO
import io.circe.{Decoder, Encoder}
import io.circe.parser._
import io.circe.generic.semiauto._

final case class Replacement(regex: String, replaceWith: String)
final case class Repo(projectId: Long, url: String)
final case class Spec(repos: List[Repo],
                      pathRegex: String,
                      branchName: String,
                      commitMessage: String,
                      mrTitle: String,
                      replacements: List[Replacement])

trait SpecReader[F[_]] {
  def readSpec(file: String): F[Spec]
}

object Replacement {
  implicit val replacementEncoder: Encoder[Replacement] = deriveEncoder[Replacement]
  implicit val replacementDecoder: Decoder[Replacement] = deriveDecoder[Replacement]
}

object Repo {
  implicit val repoEncoder: Encoder[Repo] = deriveEncoder[Repo]
  implicit val repoDecoder: Decoder[Repo] = deriveDecoder[Repo]
}

object Spec {
  implicit val specEncoder: Encoder[Spec] = deriveEncoder[Spec]
  implicit val specDecoder: Decoder[Spec] = deriveDecoder[Spec]
}

object SpecReader {
  def instance(implicit FS: FileSystem[IO]): SpecReader[IO] = new SpecReader[IO] {
    override def readSpec(specPath: String): IO[Spec] =
      for {
        content <- FS.readContent(new File(specPath))
        spec <- IO.fromEither(decode[Spec](content))
      } yield spec
  }
}
