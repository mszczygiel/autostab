package autostab

import java.io.File
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileVisitResult, Files, Path, SimpleFileVisitor}

import cats.effect.IO
import cats.syntax.all._
import cats.instances.list._
import org.slf4j.LoggerFactory

import scala.sys.process._
import autostab.LocalGitRepo.RepoUrl

import scala.util.control.NonFatal

trait LocalGitRepo[F[_]] {
  def autoUpdate(url: RepoUrl,
                 branchName: LocalGitRepo.BranchName,
                 commitMessage: LocalGitRepo.CommitMessage,
                 update: File => F[Unit]): F[LocalGitRepo.AutoUpdateResult]
}

object LocalGitRepo {
  private val log = LoggerFactory.getLogger(LocalGitRepo.getClass)

  def apply[F[_]](implicit ev: LocalGitRepo[F]): LocalGitRepo[F] = ev

  final case class RepoUrl(value: String) extends AnyVal
  final case class BranchName(value: String) extends AnyVal
  final case class CommitMessage(value: String) extends AnyVal

  sealed trait AutoUpdateResult extends Product with Serializable
  case object NoChanges extends AutoUpdateResult
  case object ChangesApplied extends AutoUpdateResult

  private sealed trait CommitResult extends Product with Serializable
  private case object CommitSuccessful extends CommitResult
  private case object NothingChanged extends CommitResult

  private[this] final case class FilesAccumulatingVisitor() extends SimpleFileVisitor[Path] {

    private val files = scala.collection.mutable.ListBuffer.empty[File]

    override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
      log.trace(s"updating $file")
      files += file.toFile
      FileVisitResult.CONTINUE
    }

    def visitedFiles: List[File] = files.toList
  }

  def instance: LocalGitRepo[IO] = new LocalGitRepo[IO] {
    override def autoUpdate(url: RepoUrl,
                            branchName: BranchName,
                            commitMessage: CommitMessage,
                            update: File => IO[Unit]): IO[LocalGitRepo.AutoUpdateResult] = {
      for {
        localDir <- mkTemp
        _ <- cloneRepository(url, localDir)
        _ <- newBranch(localDir, branchName)
        allFiles <- allFilesInTree(localDir)
        _ <- allFiles.traverse_(update)
        commitResult <- commit(localDir, commitMessage)
        updateResult <- commitResult match {
          case NothingChanged   => IO.pure(NoChanges)
          case CommitSuccessful => push(localDir, branchName) *> IO.pure(ChangesApplied)
        }
      } yield updateResult
    }

    // TODO Don't load whole tree at once
    private def allFilesInTree(root: File): IO[List[File]] = IO {
      val visitor = new FilesAccumulatingVisitor
      Files.walkFileTree(root.toPath, visitor)
      visitor.visitedFiles
    }

    private def mkTemp: IO[File] = IO {
      val file = Files.createTempDirectory("autostab").toFile
      log.debug(s"created temp directory under ${file.getAbsolutePath}")
      file.deleteOnExit() // TODO does not work for non empty directory...
      file
    }

    private def cloneRepository(url: RepoUrl, localDirectory: File): IO[Unit] = {
      IO(log.info(s"cloning ${url.value} into ${localDirectory.getAbsolutePath}")) *> runCommand(
        CloneRepository(url.value, localDirectory.getAbsolutePath))
    }

    private def newBranch(repoDir: File, branchName: BranchName): IO[Unit] = {
      runCommand(workingDirectory = repoDir, gitCommand = CheckoutNewBranch(branchName.value))
    }

    private def push(repoDir: File, branchName: BranchName): IO[Unit] = {
      IO(log.info(s"pusing ${branchName.value}")) *> runCommand(workingDirectory = repoDir,
                                                                gitCommand = Push(branchName.value))
    }

    private def commit(repoDir: File, message: CommitMessage): IO[CommitResult] = {
      runCommand(workingDirectory = repoDir, gitCommand = Commit(message.value))
        .as(CommitSuccessful: CommitResult)
        .recoverWith {
          case NonFatal(_) =>
            IO {
              log.info("Nothing to commit (no changes), or error while committing lol")
              NothingChanged: CommitResult
            }
        }
    }

    private def runCommand(workingDirectory: File, gitCommand: GitCommand): IO[Unit] =
      IO {
        val command = gitCommand.toCommand
        log.debug(s"running command $command")
        Process(command = command, cwd = workingDirectory).!!
      }.void

    private def runCommand(gitCommand: GitCommand): IO[Unit] =
      IO {
        val command = gitCommand.toCommand
        log.debug(s"running command $command")
        Process(command = command).!!
      }.void
  }

  private sealed trait GitCommand extends Product with Serializable {
    def toCommand: List[String] = this match {
      case c: CloneRepository =>
        List("git",
             "clone",
             s"${c.url}",
             "--single-branch",
             "--branch",
             "master",
             s"${c.localPath}")
      case c: CheckoutNewBranch => List("git", "checkout", "-b", c.branchName)
      case c: Push              => List("git", "push", "--set-upstream", "origin", c.branchName)
      case c: Commit            => List("git", "commit", "-am", c.message)
    }
  }
  private[this] final case class CloneRepository(url: String, localPath: String) extends GitCommand
  private[this] final case class CheckoutNewBranch(branchName: String) extends GitCommand
  private[this] final case class Push(branchName: String) extends GitCommand
  private[this] final case class Commit(message: String) extends GitCommand
}
