package autostab

import java.io.File
import java.nio.file.Files

import cats.syntax.all._
import cats.effect.IO
import org.slf4j.LoggerFactory

trait FileSystem[F[_]] {
  def updateContentIf(file: File, shouldUpdate: File => F[Boolean])(
      update: String => String): F[Unit]
  def readContent(file: File): F[String]
}

object FileSystem {
  private val log = LoggerFactory.getLogger(getClass)

  def instance: FileSystem[IO] = new FileSystem[IO] {
    override def updateContentIf(file: File, shouldUpdate: File => IO[Boolean])(
        update: String => String): IO[Unit] =
      shouldUpdate(file).ifM(doUpdate(file, update), IO.unit)

    def readContent(file: File): IO[String] = IO {
      Files.readString(file.toPath)
    }

    private def doUpdate(file: File, update: String => String) =
      for {
        _ <- IO(log.debug(s"updating $file"))
        content <- readContent(file)
        newContent = update(content)
        _ <- writeContent(file, newContent)
      } yield ()

    private def writeContent(file: File, content: String): IO[Unit] =
      IO {
        Files.writeString(file.toPath, content)
      }.void
  }
}
