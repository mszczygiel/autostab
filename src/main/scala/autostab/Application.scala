package autostab

import java.io.File

import cats.Monad
import cats.effect.{ExitCode, IO, IOApp, Resource}
import cats.instances.list._
import cats.syntax.all._
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import scopt.OParser

final case class CommandLineArgs(specFile: String = "autostab-spec.json",
                                 gitLabToken: String = "",
                                 gitLabUrl: String = "")

object Application extends IOApp {
  private val parser = {
    val builder = OParser.builder[CommandLineArgs]
    import builder._
    OParser.sequence(
      programName("autostab"),
      head("autostab"),
      note("batch update GitLab repositories"),
      opt[String]('f', "spec-file")
        .text("file with replacement specification")
        .action((v, a) => a.copy(specFile = v)),
      opt[String]('t', "token")
        .text("GitLab token")
        .required()
        .action((v, a) => a.copy(gitLabToken = v)),
      opt[String]('g', "gitlab-url")
        .text("GitLab URL, for example https://gitlab.com")
        .required()
        .action((v, a) => a.copy(gitLabUrl = v))
    )
  }

  override def run(args: List[String]): IO[ExitCode] = {
    OParser.parse(parser, args, CommandLineArgs()) match {
      case Some(config) =>
        val client: Resource[IO, Client[IO]] =
          BlazeClientBuilder[IO](scala.concurrent.ExecutionContext.Implicits.global).resource
        implicit val localGitRepo: LocalGitRepo[IO] = LocalGitRepo.instance
        implicit val fileSystem: FileSystem[IO] = FileSystem.instance
        implicit val specReader: SpecReader[IO] = SpecReader.instance
        implicit val gitLab: GitLab[IO] = GitLab.instance(GitLab.Token(config.gitLabToken),
                                                          GitLab.GitLabUrl(config.gitLabUrl),
                                                          client)
        for {
          spec <- specReader.readSpec(config.specFile)
          _ <- doUpdate[IO](spec)
        } yield ExitCode.Success
      case _ =>
        IO.pure(ExitCode.Error)
    }
  }

  def doUpdate[F[_]: Monad](
      spec: Spec)(implicit L: LocalGitRepo[F], GL: GitLab[F], FS: FileSystem[F]): F[Unit] = {
    spec.repos.traverse_ { repo =>
      L.autoUpdate(
          LocalGitRepo.RepoUrl(repo.url),
          LocalGitRepo.BranchName(spec.branchName),
          LocalGitRepo.CommitMessage(spec.commitMessage),
          file =>
            FS.updateContentIf(file, f => pathMatches(f, spec).pure[F])(content => {
              spec.replacements.foldLeft(content) { (s, replacement) =>
                s.replaceAll(replacement.regex, replacement.replaceWith)
              }
            })
        )
        .flatMap {
          case LocalGitRepo.NoChanges => ().pure[F]
          case LocalGitRepo.ChangesApplied =>
            GL.createMergeRequest(GitLab.ProjectId(repo.projectId),
                                  GitLab.BranchName(spec.branchName),
                                  GitLab.BranchName("master"),
                                  spec.mrTitle)
        }
    }
  }

  private def pathMatches(file: File, spec: Spec) = file.getAbsolutePath.matches(spec.pathRegex)
}
